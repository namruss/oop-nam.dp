<?php
require('../dao/ProductDAO.php');
class ProductDaoDemo{
    function __construct()
    {
        $this->product=new ProductDAO();
    }

    /**
     * test function insert Product Oject to ProductTable
     * @return boolean
     */
    function insert(){
        return $this->rootDatabase->insertTable('productTable',new Product(1,'Nga',1));
    }

    /**
     * test function update Product Object to ProductTable
     * @return integer
     */
    function update(){
        return $this->rootDatabase->updateTable('productTable', new Product(1,'Bap occho',2));
    }

    /**
     * test function delete Product Object to ProductTable
     * @return boolean
     */
    function delete(){
        return $this->rootDatabase->deleteTable('productTable', new Product(1,'Bap occho',3));
    }
    
    /**
     * test function truncate, delete all object on ProductTable
     * @return void 
     */
    function truncate(){
        return $this->product->truncateTable('productTable');
    }
}
?>