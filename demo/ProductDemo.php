<?php
require('./entity/Product.php');
class ProductDemo extends Product{
    function __construct() {
    }

    /**
     * test function create product object
     * @return mixed
     */
    function createProductTest(){
        $product=new Product(1,'An',5);
        return $product;
    }

    /**
     * print information of Product object
     * @return string
     */
    function printProduct(Product $product){
        return "ID Product: ".$product->getId()."| Name Product: ".$product->name;
    }
}







