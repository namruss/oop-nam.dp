<?php
require('../dao/Database.php');
class DatabaseDemo{
    function __construct()
    {
        $this->rootDatabase= Database::getInstance();
    }

    /**
     * function test insert new Product to table
     * @return boolean
     */
    function insertTableTest(){
        return $this->rootDatabase->insertTable('productTable',new Product(5,'Quac',1));  
    }

    /**
     * function test get all element on ProductTable
     * @return mixed
     */
    function selectTableTest(){
        return $this->rootDatabase->selectTable('productTable');   
    }

    /**
     * function test update product by id
     * @return integer
     */
    function updateTableTest(){
        return $this->rootDatabase->updateTable('productTable',new Product(5,"A",2));
    }

    /**
     * function test update product by id
     * @return integer
     */
    function deleteTableTest(){
        return $this->rootDatabase->deleteTable('productTable',new Product(5,'nam',2));
    }
    /**
     * function test delete all element on Product Table
     * @return void
     */
    function truncateTableTest(){
        return $this->rootDatabase->truncateTable('productTable');
    }

    function updateTableByIdTest(){
        return $this->rootDatabase->updateTableById(5,new Product(6,'nam',2));
    }

    /**
     * function insert 10 element to Product Table
     * @return void
     */
    function initDatabase(){
        for ($i=0; $i < 10 ; $i++) { 
            $this->rootDatabase->insertTable(new Product($i,'Product '.$i,1));
            $this->rootDatabase->insertTable(new Category($i,'Category '.$i,2));
            $this->rootDatabase->insertTable(new Accessory($i,'Accessory '.$i,2));
        }
    }

    /**
     * function print product table
     * @return mixed
     */
    function printDatabaseTest(){
        var_dump($this->rootDatabase->productTable);
    }

    /**
     * function test insert function
     * @return boolean
     */
    function insertTest(){
        return $this->insertTableTest();  
    }


    /**
     * function find all get table
     * @return mixed
     */
    function findAllTest(){
       return $this->selectTableTest();
    }


    /**
     * function test update function
     * @return integer
     */
    function updateTest(){
       return $this->updateTableTest();
    }
}
$new = new DatabaseDemo();
var_dump($new->insertTableTest());
var_dump($new->findAllTest());
echo "<br/>";
var_dump($new->selectTableTest());
echo "<br/>";
$new->updateTableTest();
var_dump($new->updateTableTest());
echo "<br/>";
echo "<br/>";
echo "<br/>";
var_dump($new->selectTableTest());
echo "<br/>";