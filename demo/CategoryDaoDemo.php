<?php
require('../dao/CategoryDAO.php');
class CategoryDaoDemo{
    function __construct()
    {
        $this->category=new CategoryDAO();
    }

    /**
     * test function insert Category Oject to CategoryTable
     * @return boolean
     */
    function insert(){
        return $this->category->insert(new Category(1,'Nga'));
    }

    /**
     * test function update Category Object to CategoryTable
     * @return integer
     */
    function update(){
        return $this->category->update(new Category(1,'Bap occho'));
    }

    /**
     * test function delete Category Object to CategoryTable
     * @return boolean
     */
    function delete(){
        return $this->category->delete( new Category(1,'Bap occho'));
    }
    
    /**
     * test function updateById, update Category Object to CategoryTable
     * @return boolean
     */
    function updateById(){
        return $this->category->updateById(1,new Category(5,'Hehe'));
    }

    /**
     * test function truncate, delete all object on CategoryTable 
     * @return void
     */
    function truncate(){
        return $this->category->truncateTable('categoryTable');
    }
}
?>
