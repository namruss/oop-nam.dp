<?php
require('Database.php');
require('../Interface/IDao.php');
class BaseDao implements IDao{
    function __construct()
    {
        $this->rootDatabase=Database::getInstance();
    }

    /**
     * function insert object to table
     * @param $row
     * @return boolean
     */
    function insert($row){
        $tableName=strtolower(get_class($row)).'Table';
        if($this->rootDatabase->insertTable($tableName, $row)){
            return true;
        }
        return false;
    }

    /**
     * function update object 
     * @param $row
     * @return boolean
     */
    function update($row){
        $tableName=strtolower(get_class($row)).'Table';
        if($this->rootDatabase->updateTable($tableName, $row))
        return true;
        else return false;
    }

    /**
     * function delete object to table
     * @param $row
     * @return boolean
     */
    function delete($row){
        $tableName=strtolower(get_class($row)).'Table';
        if($this->rootDatabase->deleteTable($tableName, $row)){
            return true;
        }
        return false;
    }

    /**
     * function find object by id
     * @param $id, $tableName
     * @return mixed
     */
    function findById($id,$tableName){
        for ($i = 0; $i < Count($this->rootDatabase->{$tableName}); $i++) {
            if ($this->rootDatabase->{$tableName}->getId() == $id) {
                return $this->table[$i];
            }
        }
        return null;
    }

    function search($where){

    }
}
