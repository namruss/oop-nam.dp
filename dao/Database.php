<?php
require('../entity/Product.php');
require('../entity/Category.php');
require('../entity/Accessory.php');
require('constTable.php');
class Database
{
    protected array $productTable = [];
    protected array $categoryTable = [];
    protected array $accessoryTable = [];
    private static ?Database $instants=null;   
    private function __construct()
    {
        
    }

    /**
     * function singleton design parttern Database Object 
     * @return mixed
    */

    public static function getInstance()
    {
        if(static::$instants==null){
            static::$instants=new Database();
        }
        return static::$instants;
    }

    /**
     * function check class is subclass of BaseRow class
     * @param $row
     * @return boolean
     */
    function checkSubClass($row)
    {
        if(is_subclass_of($row,'BaseRow'))
        {
            return true;
        }
        return false;
    }

    /**
     * function get data from table
     * @param $name, $id
     * @return mixed
     */
    function getDataTable($name,$id)
    {
        for ($i = 0; $i < Count($this->{$name}); $i++) {
            if ($this->{$name}[$i]->getId() == $id) {
            return $this->{$name}[$i];
            }
        }
        return null;
    }

    /**
     * function to check table name
     * @param $name
     * @return boolean
     */

    /**
     * function insert object to table
     * @param $row
     * @return integer
     */
    function insertTable($name,$row)
    {
            if($this->checkSubClass($row)){
                $this->{$name}[] = $row;
                return 1;
            }
            return 0;
    }

    /**
     * function get all element of table
     * @param $name
     * @return mixed
     */
    function selectTable($name)
    {
        if (!$this->{$name}) {
            return null;
        }
        return $this->{$name};
    }

    /**
     * function update object
     * @param $row
     * @return integer
     */
    function updateTable($name,$row)
    {
        switch ($name) {
            case PRODUCT_TABLE:
                    for ($i = 0; $i < Count($this->{$name}); $i++) {
                        if ($this->{$name}[$i]->getId() == $row->getId()) {
                        $this->{$name}[$i]=$row;
                        return 1;
                        }
                    }
                return 0;
                break;
            case CATEGORY_TABLE:
                for ($i = 0; $i < Count($this->{$name}); $i++) {
                    if ($this->{$name}[$i]->getId() == $row->getId()) {
                    $this->{$name}[$i]=$row;
                    return 1;
                    }
                }
                return 0;
                break;
            case ACCESSORY_TABLE:
                for ($i = 0; $i < Count($this->{$name}); $i++) {
                    if ($this->{$name}[$i]->getId() == $row->getId()) {
                    $this->{$name}[$i]=$row;
                    return 1;
                    }
                }
                return 0;
                break;
            default:
                break;
            }
    }

    /**
     * function update object by id
     * @param $id, $row
     * @return boolean
     */
    function updateTableById($id, $row)
    {
        $tableName=strtolower(get_class($row)).'Table';
        switch ($tableName) {
            case PRODUCT_TABLE:
                for ($i = 0; $i < Count($this->{$tableName}); $i++) {
                    if ($this->{$tableName}[$i]->getId() == $row->getId()) {
                    $this->{$tableName}[$i]=$row;
                    return 1;
                    }
                }
                return 0;
                break;
            case CATEGORY_TABLE:
                for ($i = 0; $i < Count($this->{$tableName}); $i++) {
                    if ($this->{$tableName}[$i]->getId() == $row->getId()) {
                    $this->{$tableName}[$i]=$row;
                    return 1;
                    }
                }
                return 0;
                break;
            case ACCESSORY_TABLE:
                for ($i = 0; $i < Count($this->{$tableName}); $i++) {
                    if ($this->{$tableName}[$i]->getId() == $row->getId()) {
                    $this->{$tableName}[$i]=$row;
                    return 1;
                    }
                }
                return 0;
                break;
            default:
                break;
        }
    }

    /**
     * function delete object of table
     * @param $row
     * @return boolean
     */
    function deleteTable($name,$row)
    {
        switch ($name) {
            case PRODUCT_TABLE:
                if($this->getDataTable($name,$row->getId()))
                {
                    unset($this->{$name}[$row->getId()]);
                    return true;
                };
                return false;
                break;
            case CATEGORY_TABLE:
                if($this->getDataTable($name,$row->getId()))
                {
                    unset($this->{$name}[$row->getId()]);
                    return true;
                };
                return false;
                break;
            case CATEGORY_TABLE:
                if($this->getDataTable($name,$row->getId()))
                {
                    unset($this->{$name}[$row->getId()]);
                    return true;
                };
                return false;
                break;
            default:
                # code...
                break;
        }
    }

    /**
     * function delete all element of table
     * @param $name
     * @return void
     */
    function truncateTable($name)
    {
        if($this->{$name})
        {
            unset($this->{$name});
        }
    }
}

