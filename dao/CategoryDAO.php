<?php
require('BaseDao.php');
class CategoryDAO extends BaseDao{

    /**
     * function find all element of categoryTable
     * @return mixed
     */
    function findAll(){
        return $this->rootDatabase->{'categoryTable'};
    }
}
?>
