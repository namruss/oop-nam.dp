<?php
require('Database.php');
class AccessoryDAO extends BaseDao{
    function __construct()
    {
        $this->rootDatabase=Database::getInstance();
    }

    /**
     * function find object by name
     * @param $id, $tableName
     * @return mixed
     */
    function findByName($name){
        for ($i = 0; $i < Count($this->rootDatabase->{'accessoryTable'}); $i++) {
            if ($this->rootDatabase->{'accessoryTable'}->name == $name) {
                return $this->table[$i];
            }
        }
        return null;
    }
}
?>