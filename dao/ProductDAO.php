<?php
require('Database.php');
class ProductDAO extends BaseDao{
    function __construct()
    {
        $this->rootDatabase=Database::getInstance();
    }

    /**
     * function find object by name
     * @param $id, $tableName
     * @return mixed
     */
    function findByName($name){
        for ($i = 0; $i < Count($this->rootDatabase->{'productTable'}); $i++) {
            if ($this->rootDatabase->{'productTable'}->name == $name) {
                return $this->table[$i];
            }
        }
        return null;
    }
}
?>