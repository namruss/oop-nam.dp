<?php
interface IEntity{
    /**
     * get id of object 
     * @return integer
     */
    function getId();

    /**
     * get id of object 
     * @return string
     */
    function getName();
}