<?php
interface IDao{
    /**
     * insert an object to array 
     * @param $row
     * @return boolean
     */
    function insert($row);

    /**
     * update an object by id
     * @param $row
     * @return boolean
     */
    function update($row);

    /**
     * delete an object to array
     * @param $row
     * @return boolean
     */
    function delete($row);

    /**
     * find object by id at array
     * @param $row, $tableName
     * @return mixed
     */
    function findById($id,$tableName);

    /**
     * search object
     */
    function search($where);
}