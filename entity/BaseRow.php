<?php
require('../Interface/IEnity.php');
abstract class BaseRow implements IEntity{
    protected $id;
    protected $name;
    
    /**
     * function get id
     * @return integer
     */
    function getId(){
        return $this->id;
    }

    /**
     * fucntion get name
     * @return string
     */
    function getName(){
        return $this->name;
    }

}