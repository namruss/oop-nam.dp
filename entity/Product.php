<?php
require('BaseRow.php');
class Product extends BaseRow{
    private int $categoryId;
    public function __construct($id,$name,$categoryId)
    {
        $this->id=$id;
        $this->name=$name;
        $this->categoryId=$categoryId;
    }

    /**
     * function get category id
     * @return integer
     */
    public function getCategoryId(){
        return $this->categoryId;
    }
}